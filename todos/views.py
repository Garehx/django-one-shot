from django.shortcuts import redirect, render, get_object_or_404
from todos.forms import EditForm, TodoForm, ItemForm
from todos.models import TodoItem, TodoList


# Create your views here.
def todo_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list1": todo_list,
    }
    return render(request, "todos/show.html", context)


def show_list(request, id):
    show_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": show_list,
    }
    return render(request, "todos/details.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            todos = form.save(False)
            todos.save()
            return redirect("todo_list_detail", id=todos.id)
    else:
        form = TodoForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create.html", context)


def create_item(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ItemForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create_item.html", context)


def edit_list(request, id):
    todos = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todos)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoForm(instance=todos)
    context = {
        "todo_object": todos,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def update_item(request, id):
    recipe = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = EditForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = EditForm(instance=recipe)
    context = {
        "edit_object": recipe,
        "form": form,
    }
    return render(request, "todos/edit_item.html", context)


def delete_list(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")
